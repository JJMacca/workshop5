import re
d = {}
path = open('Wood.txt')
lines = path.readlines()
for i in lines:
    i = i.upper()
    i = re.sub('[^A-Za-z]+', ' ', i)
    i = i.split()
    for j in i:
        if j in d:
            d[j] += 1 
        else:
            d[j] = 1
starTotal = sum(d.values())
finalD = [(k, d[k]) for k in sorted(d, key=d.get, reverse=True)]
for k, x in finalD:
    percent = round(x * 100/starTotal)
    if len(k) > 5:
        print('%s \t [%s]%s' % (k, (x+1) * '*', percent) + '%')
    else:
        print('%s \t\t [%s]%s' % (k, (x+1) * '*', percent) + '%')