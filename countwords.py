import re
dictonary = {}
sentence = str(input('Words: '))
sentence = sentence.upper()
sentence = re.sub('[^A-Za-z]+', ' ', sentence)
sentence = sentence.split()
for i in sentence:
    if i in dictonary:
        dictonary[i] += 1
    else:
        dictonary[i] = 1

finalDictionary = [(k, dictonary[k]) for k in sorted(dictonary, key=dictonary.get, reverse=True)]
for j, x in finalDictionary:
    print(j, x)